package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoMVC;
import model.ModelMVC;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// retriving all parameters from the JSP page
		
		String firstName = request.getParameter("FName");
		String lastName = request.getParameter("LName");
		String dob = request.getParameter("DOB");
		String gender = request.getParameter("Gender");
		String accMail = request.getParameter("mail");
		String accPwd = request.getParameter("pwd");
		
		
		HttpSession session = request.getSession();

		if(gender == null){
			 session.setAttribute("genderError", "please enter the gender");
			 response.sendRedirect("Registration.jsp");
		}
		else if(DaoMVC.getAccountsEmailsSet(accMail).contains(accMail)){
			session.setAttribute("duplicateMail", "email address is already in use");
			 response.sendRedirect("Registration.jsp");
		}
		else{
			
			// setting all values to model class
			
			ModelMVC m = new ModelMVC();
			m.setFirstName(firstName);
			m.setLastName(lastName);
			m.setDob(dob);
			m.setGender(gender);
			m.setEmail(accMail);
			m.setPassword(accPwd);
			
			//calling a method in Dao class to insert data in table
			
			try {
				DaoMVC.registerUser(m);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			 response.sendRedirect("Login.jsp");
		}

		
	
	}

}
