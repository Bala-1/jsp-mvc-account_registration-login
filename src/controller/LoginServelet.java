package controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoMVC;
import model.ModelMVC;


@WebServlet("/LoginServelet")
public class LoginServelet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();

		
		String uname = request.getParameter("LoginEmail");
		String psswd = request.getParameter("LoginPswd");
		
		ModelMVC m = new ModelMVC();

		m.setEmail(uname);
		m.setPassword(psswd);
		
			ResultSet rs = null;
			try {
				rs = DaoMVC.loginuser(m);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		
		try {
			if(rs.next()) {
				
				 String fname=rs.getString("fistName");
				 String lname=rs.getString("lastName");
				 
				 session.setAttribute("fname", fname);
				 session.setAttribute("lname", lname);
				 
				 response.sendRedirect("HomePage.jsp");
			}
			else {
				 response.sendRedirect("Login.jsp");
				 
				session.setAttribute("invalidCred", "Invalid Credentials");


			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}

}
