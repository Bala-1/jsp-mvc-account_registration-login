package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import model.ModelMVC;

public class DaoMVC {

	private static Connection getDBConnection() throws SQLException {
		String url = "jdbc:oracle:thin:@localhost:1521:xe";
		String userName = "system";
		String pwd = "admin@123";

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Connection myConn = DriverManager.getConnection(url, userName, pwd);
		return myConn;
	}

	public static void registerUser(ModelMVC m) throws SQLException {

		Connection myConn = getDBConnection();
		
		String query = "insert into accounts values(?,?,?,?,?,?)";
		PreparedStatement pst = myConn.prepareStatement(query);

		pst.setString(1, m.getFirstName());
		pst.setString(2, m.getLastName());
		pst.setString(3, m.getDob());
		pst.setString(4, m.getGender());
		pst.setString(5, m.getEmail());
		pst.setString(6, m.getPassword());

		pst.executeUpdate();
	}

	public static ResultSet loginuser(ModelMVC m) throws SQLException {
		Connection myConn = getDBConnection();
		
		String query = "select * from accounts where mail=? and pwd=?";
		PreparedStatement pst = myConn.prepareStatement(query);
		
		pst.setString(1, m.getEmail());
		pst.setString(2, m.getPassword());
		
	    ResultSet rs = pst.executeQuery();
		return rs;
	}
	
	public static Set<String> getAccountsEmailsSet(String mail) {
		Set<String> set = new HashSet<String>();
		try {
			Connection myConn = getDBConnection();
			
			
			String query = "select * from accounts where mail=?";
			
			PreparedStatement pst = myConn.prepareStatement(query);
			pst.setString(1, mail);
			
		    ResultSet rs = pst.executeQuery();
			
			 while(rs.next()) {
				 set.add(rs.getString("mail"));
			 }			
			
		}catch(Exception e) {
			e.printStackTrace();

		}
		 return set;
	}

}
