<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Account Login page</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</head>
<body>
<div class="container-fluid bg-light bg-image" 
     style="background-image: url('https://thumbs.dreamstime.com/z/beautiful-winter-landscape-scene-background-snow-covered-trees-iced-river-beauty-sunny-winter-backdrop-wonderland-frosty-126334122.jpg');
            height: 100vh">>
            
            
        <div class="signInIntro" style="text-align:center ;">
            <h1>Log in to Account</h1>
        </div>
        
		 <%  
       String invalidCred = (String)session.getAttribute("invalidCred");
       if(invalidCred != null){
       %>
		<div class="text-danger" align="center"><h2>${invalidCred} </h2> </div><br><%session.removeAttribute("invalidCred");} %>
		
        <form style=" margin-left: 410px; padding-right: 430px" action="LoginServelet" method="post">
            <div class="form-floating mb-3 mt-3">
                <input type="text" class="form-control" id="email" placeholder="Enter email" name="LoginEmail">
                <label for="email">Email</label>
            </div>
              
            <div class="form-floating mt-3 mb-3">
                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="LoginPswd">
                <label for="pwd">Password</label>
            </div>
            
                <input class="form-check-input" type="hidden" name="pagedetails" value="login">
            
            <button type="submit" class="btn btn-primary" >Log in</button>
        

    </form>

 </div>    
</body>
</html>